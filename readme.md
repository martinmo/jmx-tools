JMX Tools
=========

* Provides access to JMX via command line or scripting. 
* Can access local running JVM process of the same user (same way jconsole uses for local processes). 
* No configuration of JMX is necessary.
* JDK 1.8 is needed! (i.e. $JAVA_HOME/lib/tools.jar)

[https://bitbucket.org/martinmo/jmx-tools/downloads/JMX.jar](https://bitbucket.org/martinmo/jmx-tools/downloads/JMX.jar)

Commandline
-----------
java -cp JMX.jar:$JAVA_HOME/lib/tools.jar cmd.JMX


```
Usage:
  f <part>                                  find pid of first vm where name contains <part>
  l                                         list vms
  l <pid>                                   list objects
  l <pid> <object name>                     list methods and attributes
  f <pid> <object name pattern>             find objects
  a <pid> <object name>                     list attributes
  a <pid> <object name> <attribute>         read attribute
  w <pid> <object name> <attribute> <value> write attribute
  m <pid> <object name>                     list methods
  i <pid> <object name> <method>            invoke method
```

Commandline Example
-------------------

```
> java -cp "c:/work/jdklink/jdk8/lib/tools.jar";"target/JMX.jar" cmd.JMX l                                           
416 org.jetbrains.idea.maven.server.RemoteMavenServer
4516 cmd.JMX l
7828 C:\Work\wildfly-9.0.1.Final\jboss-modules.jar -mp C:\Work\wildfly-9.0.1.Final\modules org.jboss.as.standalone -Djboss.home.dir=C:\Work\wildfly-9.0.1.Final
8248
7436

> java -cp "c:/work/jdklink/jdk8/lib/tools.jar";"target/JMX.jar" cmd.JMX a 7828 java.lang:type=Memory HeapMemoryUsage
init: 67108864
committed: 204996608
max: 477626368
used: 46395400

> java -cp "c:/work/jdklink/jdk8/lib/tools.jar";"target/JMX.jar" cmd.JMX a 7828 java.lang:type=Memory HeapMemoryUsage used
55093632
```

Scripting
---------

jjs -scripting -cp "<path to jdk8>/lib/tools.jar";"target/JMX.jar" script.js

```
#!javascript

var JMX = Java.type('cmd.JMX');

// list vms
var vms = JMX.listVMs();
vms.forEach(function(vm){
    print(vm);
});

// connect and invoke
var pid = JMX.findPid("jboss-modules.jar");
JMX.connect(pid, function(con){
        print("got con");
        var heap = JMX.attribute(con, "java.lang:type=Memory", "HeapMemoryUsage");
        print(heap);

        print("heap used before " + heap["used"]);

        JMX.invoke(con, "java.lang:type=Memory", "gc", []);

        var heapAfterGc = JMX.attribute(con, "java.lang:type=Memory", "HeapMemoryUsage");

        print("heap used after " + heapAfterGc["used"]);
        print("freed " + (heap["used"] - heapAfterGc["used"]));

    }
);
```