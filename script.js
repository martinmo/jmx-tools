#!/usr/bin/env jjs -scripting
//#!/usr/bin/env jjs -J-Dnashorn.args=-scripting -cp "c:/work/jdklink/jdk8/lib/tools.jar";"target/jmx-script-1.0-SNAPSHOT.jar"

var JMX = Java.type('cmd.JMX');

var vms = JMX.listVMs();
vms.forEach(function(vm){
    print(vm);
});

var pid = JMX.findPid("jboss-modules.jar");

JMX.connect(pid, function(con){
        print("got con");
        var heap = JMX.attribute(con, "java.lang:type=Memory", "HeapMemoryUsage");
        print(heap);

        print("heap used before " + heap["used"]);

        JMX.invoke(con, "java.lang:type=Memory", "gc", []);

        var heapAfterGc = JMX.attribute(con, "java.lang:type=Memory", "HeapMemoryUsage");

        print("heap used after " + heapAfterGc["used"]);
        print("freed " + (heap["used"] - heapAfterGc["used"]));

    }
);
