package cmd;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class JMX {

    private static void printUsage() {
        String usage =
                "Usage:\n" +
                        "  f <part>                                  find pid of first vm where name contains <part>\n" +
                        "  l                                         list vms\n" +
                        "  l <pid>                                   list objects\n" +
                        "  l <pid> <object name>                     list methods and attributes\n" +
                        "  f <pid> <object name pattern>             find objects\n" +
                        "  a <pid> <object name>                     list attributes\n" +
                        "  a <pid> <object name> <attribute>         read attribute\n" +
                        "  w <pid> <object name> <attribute> <value> write attribute\n" +
                        "  m <pid> <object name>                     list methods\n" +
                        "  i <pid> <object name> <method> [args]     invoke method\n";
        System.out.println(usage);
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {

        if (args.length == 0) {
            printUsage();
            return;
        }

        if ("f".equals(args[0])) {
            if (args.length == 2) {
                final String pid = findPid(args[1]);
                if (pid != null) {
                    System.out.println(pid);
                } else {
                    System.out.println("NOT_FOUND");
                }
                return;
            }
        }

        if ("l".equals(args[0])) {
            if (args.length == 1) {
                listVMs().forEach(System.out::println);
                return;
            }
            if (args.length == 2) {
                connect(args[1], con -> {
                    listObjects(con).forEach(System.out::println);
                });
                return;
            }

            if (args.length == 3) {
                connect(args[1], con -> {
                    System.out.println("========= Attributes =========");
                    attributes(con, args[2]).forEach(System.out::println);
                    System.out.println("========= Methods =========");
                    methods(con, args[2]).forEach(System.out::println);
                });
                return;
            }

            printUsage();
            return;
        }

        if ("a".equals(args[0])) {
            // list
            if (args.length == 3) {
                connect(args[1], con -> {
                    final List<String> attributes = attributes(con, args[2]);
                    attributes.forEach(System.out::println);
                });
                return;
            }
            // read
            if (args.length == 4) {
                connect(args[1], con -> {
                    final Object attribute = attribute(con, args[2], args[3]);
                    final Object data = serializeCompositeData(attribute);
                    if (data instanceof Map) {
                        ((Map) data).forEach((k, v) -> System.out.println(k + ": " + v));
                    } else {
                        System.out.println(data);
                    }
                });
                return;
            }
            // read composite
            if (args.length == 5) {
                connect(args[1], con -> {
                    final Object data = attribute(con, args[2], args[3]);
                    if (data instanceof Map) {
                        System.out.println(((Map) data).get(args[4]));
                    } else {
                        System.out.println("no composite data");
                    }
                });
                return;
            }
            printUsage();
            return;
        }

        if ("w".equals(args[0])) {
            // write
            if (args.length == 5) {
                connect(args[1], con -> {
                    writeAttribute(con, args[2], args[3], args[4]);
                });
                return;
            }
            printUsage();
            return;
        }

        if ("i".equals(args[0])) {
            if (args.length >= 4) {
                connect(args[1], con -> {
                    final String[] params;
                    if (args.length > 3) {
                        params = Arrays.copyOfRange(args, 4, args.length);
                    } else {
                        params = new String[]{};
                    }
                    final Object attribute = invokeWithStringParams(con, args[2], args[3], params);
                    final Object data = serializeCompositeData(attribute);
                    if (data instanceof Map) {
                        ((Map) data).forEach((k, v) -> System.out.println(k + ": " + v));
                    } else {
                        System.out.println(data);
                    }
                });
                return;
            }
            printUsage();
            return;
        }
        if ("m".equals(args[0])) {
            if (args.length == 3) {
                connect(args[1], con -> {
                    methods(con, args[2]).forEach(System.out::println);
                });
                return;
            }
            printUsage();
            return;
        }
        if ("f".equals(args[0])) {
            if (args.length == 3) {
                connect(args[1], con -> {
                    listObjects(con, args[2]).forEach(System.out::println);
                });
                return;
            }
            printUsage();
            return;
        }

        printUsage();
    }


    public static List<String> listVMs() {
        List<VirtualMachineDescriptor> vms = VirtualMachine.list();
        return vms.stream().map(vm -> vm.id() + " " + vm.displayName()).collect(Collectors.toList());
    }

    public static String findPid(String namePart) {
        final Optional<String> first = listVMs().stream()
                .filter(name -> {
                    String self = "cmd.JMX f " + namePart;
                    if (name.toLowerCase().contains(self.toLowerCase())) {
                        return false;
                    }
                    return name.toLowerCase().contains(namePart.toLowerCase());
                }).map(name -> name.split(" ")[0]).findFirst();
        return first.orElse(null);
    }

    public static List<String> listObjects(MBeanServerConnection mBeanServerConnection) throws IOException, MalformedObjectNameException {
        final Set<ObjectName> objectNames = mBeanServerConnection.queryNames(null, null);
        return objectNames.stream()
                .map((t) -> t.getDomain() + ":" + t.getCanonicalKeyPropertyListString())
                .collect(Collectors.toList());
    }

    public static List<String> listObjects(MBeanServerConnection mBeanServerConnection, String pattern) throws IOException, MalformedObjectNameException {
        final Set<ObjectName> objectNames = mBeanServerConnection.queryNames(new ObjectName(pattern), null);
        return objectNames.stream()
                .map((t) -> t.getDomain() + ":" + t.getCanonicalKeyPropertyListString())
                .collect(Collectors.toList());
    }

    public static Object invoke(MBeanServerConnection connection, String name, String operation, Object[] params) throws Exception {

        String[] types = new String[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = params[i].getClass().getName();
        }

        return connection.invoke(new ObjectName(name), operation, params, types);
    }

    public static Object invokeWithStringParams(MBeanServerConnection connection, String name, String operation, String[] params) throws Exception {
        final ObjectName objectName = new ObjectName(name);
        final MBeanInfo mBeanInfo = connection.getMBeanInfo(objectName);
        final Optional<MBeanParameterInfo[]> foundSignature = Arrays.stream(mBeanInfo.getOperations()).filter(op -> op.getName().equals(operation)).map(MBeanOperationInfo::getSignature).findFirst();
        if (!foundSignature.isPresent()) {
            throw new IllegalArgumentException("method not found");
        }

        final MBeanParameterInfo[] signatures = foundSignature.get();
        final String[] types = new String[params.length];
        final Object[] values = new Object[params.length];
        for (int i = 0; i < params.length; i++) {
            values[i] = tryToParse(params[i], signatures[i].getType());
            types[i] = signatures[i].getType();
        }
        return connection.invoke(objectName, operation, values, types);
    }

    private static Object tryToParse(String value, String className) {
        final Object typedValue;
        if (Integer.class.getName().equals(className)) {
            typedValue = Integer.parseInt(value);
        } else if (Long.class.getName().equals(className)) {
            typedValue = Long.parseLong(value);
        } else if (Boolean.class.getName().equals(className)) {
            typedValue = Boolean.parseBoolean(value);
        } else {
            typedValue = value;
        }
        return typedValue;
    }

    public static Object attribute(MBeanServerConnection connection, String name, String attribute) throws MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, IOException {
        return serializeCompositeData(connection.getAttribute(new ObjectName(name), attribute));
    }

    public static void writeAttribute(MBeanServerConnection connection, String name, String attribute, Object value) throws MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, IOException, InvalidAttributeValueException {
        connection.setAttribute(new ObjectName(name), new Attribute(attribute, value));
    }

    public static void writeAttribute(MBeanServerConnection connection, String name, String attributeName, String value) throws MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, IOException, InvalidAttributeValueException, IntrospectionException {

        final ObjectName objectName = new ObjectName(name);
        final MBeanInfo mBeanInfo = connection.getMBeanInfo(objectName);
        final Optional<MBeanAttributeInfo> beanAttributeInfoOptional = Arrays.stream(mBeanInfo.getAttributes()).filter(a -> {
            return a.getName().equals(attributeName);
        }).findFirst();
        if (!beanAttributeInfoOptional.isPresent()) {
            throw new RuntimeException("attribute not found");
        }
        final MBeanAttributeInfo mBeanAttributeInfo = beanAttributeInfoOptional.get();

        final Object typedValue = tryToParse(value, mBeanAttributeInfo.getType());
        connection.setAttribute(objectName, new Attribute(attributeName, typedValue));
    }

    public static List<String> attributes(MBeanServerConnection connection, String name) throws MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, IOException, IntrospectionException {
        final MBeanInfo mBeanInfo = connection.getMBeanInfo(new ObjectName(name));
        return Arrays.stream(mBeanInfo.getAttributes())
                .map(attribute -> attribute.getName() + " " + attribute.getType() + "  " + attribute.getDescription())
                .collect(Collectors.toList());
    }

    public static List<String> methods(MBeanServerConnection connection, String name) throws MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, IOException, IntrospectionException {
        final MBeanInfo mBeanInfo = connection.getMBeanInfo(new ObjectName(name));
        return Arrays.stream(mBeanInfo.getOperations()).map(method -> {
            final String params = Arrays.stream(method.getSignature()).map(MBeanParameterInfo::getType).reduce((s1, s2) -> s1 + ", " + s2).orElse("");
            return method.getReturnType() + " " + method.getName() + "(" + params + ");   Description: " + method.getDescription();
        }).collect(Collectors.toList());
    }

    public static Object serializeCompositeData(Object object) {
        if (object instanceof CompositeData) {
            final CompositeData compositeData = (CompositeData) object;
            return compositeDataToMap(compositeData);
        } else {
            return object;
        }
    }

    private static Map<String, Object> compositeDataToMap(CompositeData compositeData) {
        final CompositeType compositeType = compositeData.getCompositeType();

        final Map<String, Object> attributeMap = new HashMap<>();
        compositeType.keySet().forEach(name -> {
            final Object value = compositeData.get(name);
            if (value instanceof CompositeData) {
                attributeMap.put(name, compositeDataToMap((CompositeData) value));
            } else {
                attributeMap.put(name, value);
            }
        });
        return attributeMap;
    }

    public static void connect(String pid, SaveConsumer<MBeanServerConnection> consumer) {
        try {
            final VirtualMachine attach = VirtualMachine.attach(pid);

            final String s = attach.startLocalManagementAgent();

            JMXServiceURL jmxUrl = new JMXServiceURL(s);
            JMXConnector connector = JMXConnectorFactory.connect(jmxUrl);
            if (connector == null) {
                throw new RuntimeException("failed to connect to vm. pid: " + pid);
            }
            final MBeanServerConnection mBeanServerConnection = connector.getMBeanServerConnection();

            try {
                consumer.accept(mBeanServerConnection);
            } finally {
                connector.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @FunctionalInterface
    public interface SaveConsumer<T> {
        void accept(T t) throws Exception;
    }
}
