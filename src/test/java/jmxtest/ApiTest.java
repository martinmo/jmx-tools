package jmxtest;

import cmd.JMX;
import org.assertj.core.api.Condition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static cmd.JMX.connect;
import static org.assertj.core.api.Assertions.assertThat;

public class ApiTest {

    private TestBean testBean;

    private String pid() {
        return ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
    }

    @Before
    public void registerMBean() throws NotCompliantMBeanException, InstanceAlreadyExistsException, MalformedObjectNameException, MBeanRegistrationException {
        testBean = TestBean.register();
    }

    @After
    public void unresgister() throws MalformedObjectNameException, InstanceNotFoundException, MBeanRegistrationException {
        TestBean.unregister();
    }

    @Test
    public void canConnect() {
        AtomicBoolean connected = new AtomicBoolean(false);
        connect(pid(), c -> {
            final List<String> objects = JMX.listObjects(c);
            System.out.println(objects.stream().collect(Collectors.joining("\n")));
            assertThat(objects).isNotEmpty();
            assertThat(objects).contains(TestBean.OBJECT_NAME);
            connected.set(true);
        });

        assertThat(connected.get()).isTrue();
    }

    @Test
    public void canList() {
        final List<String> vMs = JMX.listVMs();
        vMs.forEach(System.out::println);
        assertThat(vMs.size()).isGreaterThanOrEqualTo(2);
    }

    @Test
    public void canReadWriteAttribute() {
        connect(pid(), c -> {
            cmd.JMX.writeAttribute(c, TestBeanMBean.OBJECT_NAME, "StringValue", "1");
            final Object stringValue = JMX.attribute(c, TestBeanMBean.OBJECT_NAME, "StringValue");
            assertThat("1").isEqualTo(stringValue);
        });
    }

    @Test
    public void canInvoke() {
        connect(pid(), c -> {
            final Object result = JMX.invoke(c, TestBeanMBean.OBJECT_NAME, "revert", new Object[]{"123"});
            assertThat("321").isEqualTo(result);
        });
    }

    @Test
    public void canInvokeWithMagic() {
        connect(pid(), c -> {
            final Object result = JMX.invokeWithStringParams(c, TestBeanMBean.OBJECT_NAME, "revert", new String[]{"123"});
            assertThat("321").isEqualTo(result);
        });
    }

    @Test
    public void canReadWriteLongAttribute() {
        connect(pid(), c -> {
            cmd.JMX.writeAttribute(c, TestBeanMBean.OBJECT_NAME, "LongValue", "1");
            final Object longValue = JMX.attribute(c, TestBeanMBean.OBJECT_NAME, "LongValue");
            assertThat(1L).isEqualTo(longValue);
        });
    }

    @Test
    public void canReadWriteObjectAttribute() {
        connect(pid(), c -> {
            cmd.JMX.writeAttribute(c, TestBeanMBean.OBJECT_NAME, "LongValue", Long.valueOf(1));
            final Object longValue = JMX.attribute(c, TestBeanMBean.OBJECT_NAME, "LongValue");
            assertThat(1L).isEqualTo(longValue);
        });
    }

    @Test
    public void canReadWriteIntegerAttribute() {
        connect(pid(), c -> {
            cmd.JMX.writeAttribute(c, TestBeanMBean.OBJECT_NAME, "IntegerValue", "1");
            final Object longValue = JMX.attribute(c, TestBeanMBean.OBJECT_NAME, "IntegerValue");
            assertThat(1).isEqualTo(longValue);
        });
    }
    @Test
    public void canListFilteredObjects() {
        connect(pid(), c -> {
            final List<String> objects = JMX.listObjects(c, TestBeanMBean.OBJECT_NAME);
            assertThat(objects).containsExactly(TestBean.OBJECT_NAME);
        });
    }

    @Test
    public void canListMethods() {
        connect(pid(), c -> {
            final List<String> objects = JMX.methods(c, TestBeanMBean.OBJECT_NAME);
            assertThat(objects).hasSize(1);
            assertThat(objects.get(0)).contains("revert");
        });
    }

    @Test
    public void canListAttributes() {
        connect(pid(), c -> {
            final List<String> objects = JMX.attributes(c, TestBeanMBean.OBJECT_NAME);
            assertThat(objects).areExactly(1,new Condition<String>(){
                @Override
                public boolean matches(String value) {
                    return value.contains("LongValue");
                }
            });
        });
    }

    @Test
    public void canReadCompositeAttribute() {
        connect(pid(), c -> {
            final List<String> objects = JMX.attributes(c, "java.lang:type=Memory");
            final Map<String, Object> attribute = (Map<String, Object>) JMX.attribute(c, "java.lang:type=Memory","HeapMemoryUsage");
            System.out.println(attribute);
            assertThat(attribute).containsKeys("init","committed","max","used");
        });
    }

}
