package jmxtest;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class TestBean implements TestBeanMBean {

    private String stringValue = "stringValue";
    private Long longValue;
    private Integer integerValue;

    public TestBean() {
    }

    public static void main(String[] args) throws Exception {
        register();
        Thread.sleep(Long.MAX_VALUE);
    }

    public static TestBean register() throws NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
        final TestBean test = new TestBean();

        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        ObjectName objectName = new ObjectName(TestBeanMBean.OBJECT_NAME);
        server.registerMBean(new TestBean(), objectName);

        return test;
    }

    public static void unregister() throws MalformedObjectNameException, MBeanRegistrationException, InstanceNotFoundException {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        ObjectName objectName = new ObjectName(TestBeanMBean.OBJECT_NAME);
        server.unregisterMBean(objectName);
    }

    @Override
    public Long getLongValue() {
        return longValue;
    }

    @Override
    public void setLongValue(Long longValue) {
        this.longValue = longValue;
    }

    @Override
    public String getStringValue() {

        return stringValue;
    }

    @Override
    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String revert(String text) {
        return new StringBuilder(text).reverse().toString();
    }

    @Override
    public Integer getIntegerValue() {
        return integerValue;
    }

    @Override
    public void setIntegerValue(Integer integerValue) {
        this.integerValue = integerValue;
    }
}
