package jmxtest;

public interface TestBeanMBean {

    String OBJECT_NAME = "jmxtest:type=TestMBean";

    Long getLongValue();

    void setLongValue(Long longValue);

    String getStringValue();

    void setStringValue(String stringValue);

    String revert(String text);

    Integer getIntegerValue();

    void setIntegerValue(Integer integerValue);
}
